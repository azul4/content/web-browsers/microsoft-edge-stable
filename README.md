# microsoft-edge-stable

A browser that combines a minimal design with sophisticated technology to make the web faster, safer, and easier

https://packages.microsoft.com/yumrepos/edge/Packages/m/

<br>

How to clone this repository:
```
git cone https://gitlab.com/azul4/content/web-browsers/microsoft-edge-stable.git
```

