# Maintainer: Nicolas Narvaez <nicomix1006@gmail.com>
# Contributor: EsauPR
# Contributor: bittin
# Modified by Rafael from azul

pkgname=microsoft-edge-stable
pkgver=114.0.1823.67
pkgrel=1
pkgdesc="A browser that combines a minimal design with sophisticated technology to make the web faster, safer, and easier"
arch=('x86_64')
url="https://www.microsoftedgeinsider.com/en-us/download"
urldown="https://packages.microsoft.com/yumrepos/edge/Packages/m/"
license=('custom')
provides=('microsoft-edge-stable')
conflicts=('microsoft-edge-stable-bin')
depends=('gtk3' 'libcups' 'nss' 'alsa-lib' 'libxtst' 'libdrm' 'mesa')
makedepends=('imagemagick')
optdepends=('libpipewire02: WebRTC desktop sharing under Wayland'
            'kdialog: for file dialogs in KDE'
            'gnome-keyring: for storing passwords in GNOME keyring'
            'kwallet: for storing passwords in KWallet'
            'libunity: for download progress on KDE'
            'ttf-liberation: fix fonts for some PDFs - CRBug #369991'
            'xdg-utils')
options=(!strip !zipman)
_channel=stable
source=("${urldown}/${pkgname}-${pkgver}-1.x86_64.rpm"
        "microsoft-edge-stable.sh"
        "Microsoft Standard Application License Terms - Standalone (free) Use Terms.pdf")
sha256sums=('caa2f2298386eb1d0321e96652ad05af0d30689c1995b4272dc54c477e595ff2'
            'dc3765d2de6520b13f105b8001aa0e40291bc9457ac508160b23eea8811e26af'
            'edf2ed596eb068f168287fc76aa713ad5e0afb59f0a0a47a4f29c0c124ade15e')

package() {
	# bsdtar -xf data.tar.xz -C "$pkgdir/"
	mkdir -p ${pkgdir}/etc
	mkdir -p ${pkgdir}/opt
	mkdir -p ${pkgdir}/usr
	cp -r ${srcdir}/etc/* ${pkgdir}/etc
	cp -r ${srcdir}/opt/* ${pkgdir}/opt
	cp -r ${srcdir}/usr/* ${pkgdir}/usr

	# suid sandbox
	chmod 4755 "${pkgdir}/opt/microsoft/msedge/msedge-sandbox"

	# 256 and 24 are proper colored icons
	for res in 128 64 48 32; do
		convert "${pkgdir}/opt/microsoft/msedge/product_logo_256.png" \
			-resize ${res}x${res} \
			"${pkgdir}/opt/microsoft/msedge/product_logo_${res}.png"
	done
	for res in 22 16; do
		convert "${pkgdir}/opt/microsoft/msedge/product_logo_24.png" \
			-resize ${res}x${res} \
			"${pkgdir}/opt/microsoft/msedge/product_logo_${res}.png"
	done

	# install icons
	for res in 16 22 24 32 48 64 128 256; do
		install -Dm644 "${pkgdir}/opt/microsoft/msedge/product_logo_${res}.png" \
			"${pkgdir}/usr/share/icons/hicolor/${res}x${res}/apps/microsoft-edge.png"
	done
       # User flag aware launcher
       install -m755 microsoft-edge-stable.sh "${pkgdir}/usr/bin/microsoft-edge-stable"

	# License
	install -Dm644 'Microsoft Standard Application License Terms - Standalone (free) Use Terms.pdf' "${pkgdir}/usr/share/licenses/microsoft-edge/LICENSE.pdf"
	# rm -r "${pkgdir}/etc/cron.daily/" "${pkgdir}/opt/microsoft/msedge/cron/"
	# Globbing seems not to work inside double parenthesis
	rm "${pkgdir}/opt/microsoft/msedge"/product_logo_*.png
}
